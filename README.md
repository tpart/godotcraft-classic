# godotcraft-classic
A block game client compatible with Minecraft Classic made in Godot Mono

**IMPORTANT: At the moment, only Godot 3 LTS is supported. Some work has been done to port the project to Godot 4 on the `4.0` branch, but it is not yet fully functional.**

## Building
- Download [Godot 3 LTS Mono](https://godotengine.org/download/3.x/linux/)
- Install [.NET SDK](https://dotnet.microsoft.com/en-us/download)
- Open the project in the Godot Editor and press play

## Providing an asset pack
- Godotcraft Classic requires an asset pack (textures, sounds, etc.) to work correctly
- I recommend using the [Minecraft 1.18.2 assets](https://github.com/InventivetalentDev/minecraft-assets/archive/refs/heads/1.18.2.zip)

## Connecting to a server
- A Minecraft Classic server is not included
- I personally recommend [classicl-server](https://gitlab.com/nokoe/classicl-server), but any Minecraft Classic server should work fine
- Press "Multiplayer", "Direct Connect" and enter the server IP (If the server is running on your local computer, the IP should be "localhost"), press "Join Server"

## License
This project is licensed under the GPL-3.0-or-later license.


godotcraft-classic
Copyright (C) 2022 tpart

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.