extends Control


const SERVER_LIST_ITEM = preload("res://scenes/server_list_item.tscn")

var selected_server := ""
var last_selected_server := ""

onready var text_field_name = $edit_server_info/text_fields/text_fields/center/server_name/text_field
onready var text_field_address = $edit_server_info/text_fields/text_fields/center2/server_address/text_field
onready var text_field_address_direct = $direct_connect/text_fields/text_fields/center2/server_address/text_field
onready var server_list = $server_selection/server_list/scroll/list


func _ready():
	# Set up background texture
	var dirt = Global.load_image_texture(Global.texture_dir + "gui/options_background.png")
	$bg.texture = Global.get_resized_texture(dirt, dirt.get_size().x * 4 * Global.settings["gui_scale"], dirt.get_size().y * 4 * Global.settings["gui_scale"])
	# Load server list
	for i in Global.settings["servers"].keys():
		add_server(i, Global.settings["servers"][i])


func add_server(server_name, server_address):
	var cc = CenterContainer.new()
	cc.name = server_name
	server_list.add_child(cc)
	var item = SERVER_LIST_ITEM.instance()
	cc.add_child(item)
	item.connect("move_up", self, "_item_move_up")
	item.connect("move_down", self, "_item_move_down")
	item.connect("join_server", self, "join_server")
	item.set_up(server_name, server_address)


func _on_cancel_pressed():
	Audio.play_sound(Global.load_sound(Global.sound_dir + "random/click_stereo.ogg"))
	Global.try(get_tree().change_scene("res://scenes/main_menu.tscn"))


func _on_add_server_pressed():
	Audio.play_sound(Global.load_sound(Global.sound_dir + "random/click_stereo.ogg"))
	$server_selection.hide()
	$edit_server_info.show()


func _process(_delta):
	if text_field_address.get_value().length() > 0 and text_field_name.get_value().length() > 0:
		$edit_server_info/buttons/vbox/center/done_edit.enable()
	else:
		$edit_server_info/buttons/vbox/center/done_edit.disable()
	if text_field_address_direct.get_value().length() > 0:
		$direct_connect/buttons/vbox/center/join_server_direct.enable()
	else:
		$direct_connect/buttons/vbox/center/join_server_direct.disable()
	# Check if server is selected
	selected_server = ""
	for i in server_list.get_children():
		if i.get_child(0).has_focus():
			selected_server = i.get_child(0).server_name
			break
	if selected_server != "":
		last_selected_server = selected_server
	# Enable buttons
	if selected_server == "" and $server_selection/buttons/vbox/row1/join_server.button_enabled:
		$server_selection/buttons/vbox/row1/join_server.disable()
		$server_selection/buttons/vbox/row2/edit.disable()
		$server_selection/buttons/vbox/row2/delete.disable()
	elif selected_server != "" and $server_selection/buttons/vbox/row1/join_server.button_enabled == false:
		$server_selection/buttons/vbox/row1/join_server.enable()
		$server_selection/buttons/vbox/row2/edit.enable()
		$server_selection/buttons/vbox/row2/delete.enable()


func _on_cancel_edit_pressed():
	Audio.play_sound(Global.load_sound(Global.sound_dir + "random/click_stereo.ogg"))
	text_field_name.reset_text()
	text_field_address.reset_text()
	$server_selection.show()
	$edit_server_info.hide()


func _on_done_edit_pressed():
	Audio.play_sound(Global.load_sound(Global.sound_dir + "random/click_stereo.ogg"))
	add_server(text_field_name.get_value(), text_field_address.get_value())
	Global.settings["servers"][text_field_name.get_value()] = text_field_address.get_value()
	Global.save_settings()
	text_field_name.reset_text()
	text_field_address.reset_text()
	$server_selection.show()
	$edit_server_info.hide()


func _on_cancel_direct_pressed():
	Audio.play_sound(Global.load_sound(Global.sound_dir + "random/click_stereo.ogg"))
	text_field_address_direct.reset_text()
	$server_selection.show()
	$direct_connect.hide()


func _on_direct_connect_pressed():
	Audio.play_sound(Global.load_sound(Global.sound_dir + "random/click_stereo.ogg"))
	$server_selection.hide()
	$direct_connect.show()


func _on_delete_pressed():
	Audio.play_sound(Global.load_sound(Global.sound_dir + "random/click_stereo.ogg"))
	if last_selected_server != "":
		if Global.settings["servers"].has(last_selected_server):
			Global.settings["servers"].erase(last_selected_server)
			Global.save_settings()
			for i in server_list.get_children():
				if i.get_child(0).server_name == last_selected_server:
					i.queue_free()
					break
			last_selected_server = ""


func _on_refresh_pressed():
	Audio.play_sound(Global.load_sound(Global.sound_dir + "random/click_stereo.ogg"))
	for i in server_list.get_children():
		i.get_child(0).refresh()


func _item_move_up(item):
	if item.get_parent().get_position_in_parent() - 1 >= 0:
		server_list.move_child(item.get_parent(), item.get_parent().get_position_in_parent() - 1)


func _item_move_down(item):
	if item.get_parent().get_position_in_parent() + 1 <= server_list.get_child_count():
		server_list.move_child(item.get_parent(), item.get_parent().get_position_in_parent() + 1)


func join_server(address):
	Audio.play_sound(Global.load_sound(Global.sound_dir + "random/click_stereo.ogg"))
	Global.server_address = address
	Audio.stop_menu_music()
	Audio.autoplay_survival_overworld_music()
	Global.try(get_tree().change_scene("res://scenes/multiplayer.tscn"))


func _on_join_server_direct_pressed():
	join_server(text_field_address_direct.get_value())


func _on_join_server_pressed():
	Audio.play_sound(Global.load_sound(Global.sound_dir + "random/click_stereo.ogg"))
	# Get server address
	var server_ip
	for i in server_list.get_children():
		if i.get_child(0).server_name == last_selected_server:
			server_ip = i.get_child(0).server_address
			break
	if server_ip != null:
		join_server(server_ip)


func _on_edit_pressed():
	Global.not_yet_implemented()
