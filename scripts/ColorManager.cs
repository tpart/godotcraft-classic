using Godot;
using System.Linq;
using System.Collections.Generic;

public class ColorManager : Node
{
    public static Dictionary<string, Color> BackgroundColorCodes = new Dictionary<string, Color>() {
        ["&0"] = CreateGodotColor(0, 0, 0),
        ["&1"] = CreateGodotColor(0, 0, 47),
        ["&2"] = CreateGodotColor(0, 47, 0),
        ["&3"] = CreateGodotColor(0, 47, 47),
        ["&4"] = CreateGodotColor(47, 0, 0),
        ["&5"] = CreateGodotColor(47, 0, 47),
        ["&6"] = CreateGodotColor(47, 47, 0),
        ["&7"] = CreateGodotColor(47, 47, 47),
        ["&8"] = CreateGodotColor(16, 16, 16),
        ["&9"] = CreateGodotColor(16, 16, 63),
        ["&a"] = CreateGodotColor(16, 63, 16),
        ["&b"] = CreateGodotColor(16, 63, 63),
        ["&c"] = CreateGodotColor(63, 16, 16),
        ["&d"] = CreateGodotColor(63, 16, 63),
        ["&e"] = CreateGodotColor(63, 63, 16),
        ["&f"] = CreateGodotColor(63, 63, 63),
    };
    public static Dictionary<string, Color> ForegroundColorCodes = new Dictionary<string, Color>() {
        ["&0"] = CreateGodotColor(0, 0, 0),
        ["&1"] = CreateGodotColor(0, 0, 191),
        ["&2"] = CreateGodotColor(0, 191, 0),
        ["&3"] = CreateGodotColor(0, 191, 191),
        ["&4"] = CreateGodotColor(191, 0, 0),
        ["&5"] = CreateGodotColor(191, 0, 191),
        ["&6"] = CreateGodotColor(191, 191, 0),
        ["&7"] = CreateGodotColor(191, 191, 191),
        ["&8"] = CreateGodotColor(64, 64, 64),
        ["&9"] = CreateGodotColor(64, 64, 255),
        ["&a"] = CreateGodotColor(64, 255, 64),
        ["&b"] = CreateGodotColor(64, 255, 255),
        ["&c"] = CreateGodotColor(255, 64, 64),
        ["&d"] = CreateGodotColor(255, 64, 255),
        ["&e"] = CreateGodotColor(255, 255, 64),
        ["&f"] = CreateGodotColor(255, 255, 255),
    };

    public static Color CreateGodotColor(int r, int g, int b)
    {
        // Godot Colors are stupid
        return new Color((float)r / 255f, (float)g / 255f, (float)b / 255f);
    }

    public static Color GetForegroundColor(string colorCode)
    {
        return ForegroundColorCodes[colorCode];
    }

    public static Color GetBackgroundColor(string colorCode)
    {
        return BackgroundColorCodes[colorCode];
    }

    public static string GetForegroundColorHex(string colorCode)
    {
        return ForegroundColorCodes[colorCode].ToHtml(false);
    }

    public static string GetBackgroundColorHex(string colorCode)
    {
        return BackgroundColorCodes[colorCode].ToHtml(false);
    }

    public static string[] GetColorCodes()
    {
        return ForegroundColorCodes.Keys.ToArray();
    }
}
