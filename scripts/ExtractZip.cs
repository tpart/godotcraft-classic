using Godot;
using System;
using System.IO.Compression;
using System.Threading.Tasks;

public class ExtractZip : Node
{
	[Signal]
	delegate void ExtractionFinished();
	private Task _t;

	public void Extract(string zipPath, string filePath)
	{
		ZipFile.ExtractToDirectory(zipPath, filePath);
	}

	public void ExtractAsync(string zipPath, string filePath)
	{
		GD.Print($"Starting Extraction: {zipPath}, {filePath}!");
		_t = Task.Factory.StartNew(() => ExtractWithSignal(zipPath, filePath));
	}

	public void ExtractWithSignal(string zipPath, string filePath)
	{
		ZipFile.ExtractToDirectory(zipPath, filePath);
		EmitSignal("ExtractionFinished", filePath);
	}
}
