using System;
using classy.Data;
using System.Collections.Generic;
using Godot;

namespace Godotcraft
{
    public class BlockTextures : Node
    {
        // SingleBlockMaterials: For blocks that have the same texture on all sides
        public Dictionary<Block, SpatialMaterial> SingleBlockMaterials = new Dictionary<Block, SpatialMaterial>();
        // MultipleBlockMaterials: For blocks that have different textures on the top, side and bottom. Order: (top, side, bottom)
        public Dictionary<Block, (SpatialMaterial, SpatialMaterial, SpatialMaterial)> MultipleBlockMaterials = new Dictionary<Block, (SpatialMaterial, SpatialMaterial, SpatialMaterial)>();

        public override void _Ready()
        {
            SingleBlockMaterials.Add(Block.Stone, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("stone"), MetallicSpecular = 0 });
            MultipleBlockMaterials.Add(Block.Grass, ( new SpatialMaterial() { AlbedoColor = new Color("7cbd6b"), MetallicSpecular = 0, AlbedoTexture = LoadBlockTexture("grass_block_top") }, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("grass_block_side"), MetallicSpecular = 0 }, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("dirt"), MetallicSpecular = 0 } ));
            SingleBlockMaterials.Add(Block.Dirt, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("dirt"), MetallicSpecular = 0 });
            SingleBlockMaterials.Add(Block.Cobblestone, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("cobblestone"), MetallicSpecular = 0 });
            SingleBlockMaterials.Add(Block.Wood, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("oak_planks"), MetallicSpecular = 0 });
            SingleBlockMaterials.Add(Block.Sapling, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("oak_sapling"), MetallicSpecular = 0, FlagsTransparent = true, ParamsDepthDrawMode = SpatialMaterial.DepthDrawMode.AlphaOpaquePrepass, ParamsCullMode = SpatialMaterial.CullMode.Disabled });
            SingleBlockMaterials.Add(Block.Bedrock, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("bedrock"), MetallicSpecular = 0 });
            SingleBlockMaterials.Add(Block.Water, new SpatialMaterial() { AlbedoColor = new Color("3F76E4"), AlbedoTexture = LoadResizedBlockTexture("water_flow", 16, 16), MetallicSpecular = 0, FlagsTransparent = true, ParamsDepthDrawMode = SpatialMaterial.DepthDrawMode.AlphaOpaquePrepass });
            SingleBlockMaterials.Add(Block.StillWater, new SpatialMaterial() { AlbedoColor = new Color("3F76E4"), AlbedoTexture = LoadResizedBlockTexture("water_still", 16, 16), MetallicSpecular = 0, FlagsTransparent = true, ParamsDepthDrawMode = SpatialMaterial.DepthDrawMode.AlphaOpaquePrepass });
            SingleBlockMaterials.Add(Block.Lava, new SpatialMaterial() { AlbedoTexture = LoadResizedBlockTexture("lava_flow", 16, 16), MetallicSpecular = 0 });
            SingleBlockMaterials.Add(Block.StillLava, new SpatialMaterial() { AlbedoTexture = LoadResizedBlockTexture("lava_still", 16, 16), MetallicSpecular = 0 });
            SingleBlockMaterials.Add(Block.Sand, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("sand"), MetallicSpecular = 0 });
            SingleBlockMaterials.Add(Block.Gravel, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("gravel"), MetallicSpecular = 0 });
            SingleBlockMaterials.Add(Block.GoldOre, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("gold_ore"), MetallicSpecular = 0 });
            SingleBlockMaterials.Add(Block.IronOre, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("iron_ore"), MetallicSpecular = 0 });
            SingleBlockMaterials.Add(Block.CoalOre, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("coal_ore"), MetallicSpecular = 0 });
            SingleBlockMaterials.Add(Block.Log, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("oak_log"), MetallicSpecular = 0 });
            SingleBlockMaterials.Add(Block.Leaves, new SpatialMaterial() { AlbedoColor = new Color("77ab2f"), MetallicSpecular = 0, AlbedoTexture = LoadBlockTexture("oak_leaves"), FlagsTransparent = true, ParamsDepthDrawMode = SpatialMaterial.DepthDrawMode.AlphaOpaquePrepass });
            SingleBlockMaterials.Add(Block.Sponge, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("sponge"), MetallicSpecular = 0 });
            SingleBlockMaterials.Add(Block.Glass, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("glass"), MetallicSpecular = 0, FlagsTransparent = true, ParamsDepthDrawMode = SpatialMaterial.DepthDrawMode.AlphaOpaquePrepass });
            SingleBlockMaterials.Add(Block.Red, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("red_wool"), MetallicSpecular = 0 });
            SingleBlockMaterials.Add(Block.Orange, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("orange_wool"), MetallicSpecular = 0 });
            SingleBlockMaterials.Add(Block.Yellow, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("yellow_wool"), MetallicSpecular = 0 });
            SingleBlockMaterials.Add(Block.Lime, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("lime_wool"), MetallicSpecular = 0 });
            SingleBlockMaterials.Add(Block.Green, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("green_wool"), MetallicSpecular = 0 });
            SingleBlockMaterials.Add(Block.Teal, new SpatialMaterial() { AlbedoColor = new Color("39fa9b"), MetallicSpecular = 0, AlbedoTexture = LoadBlockTexture("white_wool") });
            SingleBlockMaterials.Add(Block.Aqua, new SpatialMaterial() { AlbedoColor = new Color("35ebeb"), MetallicSpecular = 0, AlbedoTexture = LoadBlockTexture("white_wool") });
            SingleBlockMaterials.Add(Block.Cyan, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("cyan_wool"), MetallicSpecular = 0 });
            SingleBlockMaterials.Add(Block.Blue, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("blue_wool"), MetallicSpecular = 0 });
            SingleBlockMaterials.Add(Block.Indigo, new SpatialMaterial() { AlbedoColor = new Color("4b0082"), MetallicSpecular = 0, AlbedoTexture = LoadBlockTexture("white_wool") });
            SingleBlockMaterials.Add(Block.Violet, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("purple_wool"), MetallicSpecular = 0 });
            SingleBlockMaterials.Add(Block.Magenta, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("magenta_wool"), MetallicSpecular = 0 });
            SingleBlockMaterials.Add(Block.Pink, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("pink_wool"), MetallicSpecular = 0 });
            SingleBlockMaterials.Add(Block.Black, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("black_wool"), MetallicSpecular = 0 });
            SingleBlockMaterials.Add(Block.Gray, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("gray_wool"), MetallicSpecular = 0 });
            SingleBlockMaterials.Add(Block.White, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("white_wool"), MetallicSpecular = 0 });
            SingleBlockMaterials.Add(Block.Dandelion, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("dandelion"), MetallicSpecular = 0, FlagsTransparent = true, ParamsDepthDrawMode = SpatialMaterial.DepthDrawMode.AlphaOpaquePrepass, ParamsCullMode = SpatialMaterial.CullMode.Disabled });
            SingleBlockMaterials.Add(Block.Rose, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("poppy"), MetallicSpecular = 0, FlagsTransparent = true, ParamsDepthDrawMode = SpatialMaterial.DepthDrawMode.AlphaOpaquePrepass, ParamsCullMode = SpatialMaterial.CullMode.Disabled });
            SingleBlockMaterials.Add(Block.BrownMushroom, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("brown_mushroom_block"), MetallicSpecular = 0 });
            SingleBlockMaterials.Add(Block.RedMushroom, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("red_mushroom_block"), MetallicSpecular = 0 });
            SingleBlockMaterials.Add(Block.Gold, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("gold_block"), MetallicSpecular = 0 });
            SingleBlockMaterials.Add(Block.Iron, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("iron_block"), MetallicSpecular = 0 });
            SingleBlockMaterials.Add(Block.DoubleSlab, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("smooth_stone_slab_side"), MetallicSpecular = 0 });
            SingleBlockMaterials.Add(Block.Slab, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("stone"), MetallicSpecular = 0 });
            SingleBlockMaterials.Add(Block.Brick, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("bricks"), MetallicSpecular = 0 });
            SingleBlockMaterials.Add(Block.TNT, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("tnt_side"), MetallicSpecular = 0 });
            SingleBlockMaterials.Add(Block.Bookshelf, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("bookshelf"), MetallicSpecular = 0 });
            SingleBlockMaterials.Add(Block.MossyRocks, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("mossy_cobblestone"), MetallicSpecular = 0 });
            SingleBlockMaterials.Add(Block.Obsidian, new SpatialMaterial() { AlbedoTexture = LoadBlockTexture("obsidian"), MetallicSpecular = 0 });
        }

        public ImageTexture LoadBlockTexture(string blockName)
        {
            Node global = GetNode("/root/Global");
            string dir = (string)global.Get("texture_dir") + $"block/{blockName}.png";
            return (ImageTexture)global.Call("load_image_texture", dir);
        }

        public ImageTexture LoadResizedBlockTexture(string blockName, int x, int y)
        {
            Node global = GetNode("/root/Global");
            string dir = (string)global.Get("texture_dir") + $"block/{blockName}.png";
            return (ImageTexture)global.Call("get_resized_texture", (ImageTexture)global.Call("load_image_texture", dir), x, y);
        }
    }
}