extends Spatial


func set_up_textures(front, right, back, left, up, down):
	$front.material_override = create_material(front)
	$right.material_override = create_material(right)
	$back.material_override = create_material(back)
	$left.material_override = create_material(left)
	$up.material_override = create_material(up)
	$down.material_override = create_material(down)

func create_material(texture) -> SpatialMaterial:
	var mat = SpatialMaterial.new()
	mat.flags_unshaded = true
	mat.albedo_texture = texture
	return mat
