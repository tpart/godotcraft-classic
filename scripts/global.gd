extends Node


const VERSION = "1.0"
const MC_VERSION = "Classic"
const PROTOCOL_VERSION = 758
const SAVE_PATH_SETTINGS = "user://settings.json"
const DEFAULT_FONT = preload("res://assets/fonts/vegur/Vegur-Regular.otf")
const DEFAULT_SETTINGS = {
	"assets_dir" : null,
	"font_dir" : null,
	"gui_scale" : 1,
	"fullscreen" : false,
	"servers" : {},
}
const FALLBACK_SOUND = preload("res://assets/sounds/missing_sound.wav")
const FALLBACK_TEXTURE = preload("res://assets/textures/missing_texture.png")

var assets_dir = ""
var texture_dir = ""
var music_dir = ""
var sound_dir = ""
var shader_dir = ""
var font_data_dir = ""
var font_texture_dir = ""
var font = ""
var server_address = ""
var connection_error = ""
var has_shown_broken_asset_pack_message := false
var settings = DEFAULT_SETTINGS


func _ready():
	var directory = Directory.new()
	var files = []
	directory.open("user://")
	directory.list_dir_begin()
	while true:
		var file = directory.get_next()
		if file == "":
			break
		elif not file.begins_with("."):
			files.append(file)
	directory.list_dir_end()
	for i in files:
		if i.begins_with("minecraft"):
			# Start game
			Global.assets_dir = "user://" + i + "/"
			Global.prepare_assets()


func prepare_assets():
	refresh_dirs()
	prepare_fonts()


func load_image_texture(path):
	var img = Image.new()
	var error = img.load(path)
	if error != OK:
		push_warning("Can not open texture file at %s" % path)
		missing_ressource()
		var tex = ImageTexture.new()
		tex.create_from_image(FALLBACK_TEXTURE, 1)
		return tex
	var tex = ImageTexture.new()
	tex.create_from_image(img, 1)
	return tex


func load_sound(path):
	var file = File.new()
	if not file.file_exists(path):
		push_warning("Can not open sound file at %s" % path)
		missing_ressource()
		return FALLBACK_SOUND
	file.open(path, File.READ)
	var bytes = file.get_buffer(file.get_len())
	var sound = AudioStreamOGGVorbis.new()
	sound.data = bytes
	file.close()
	return sound


func load_json(path):
	var dict = {}
	var file = File.new()
	if not file.file_exists(path):
		push_warning("Can not open json file at %s" % path)
		missing_ressource()
		return {}
	file.open(path, file.READ)
	var text = file.get_as_text()
	dict = parse_json(text)
	file.close()
	return dict


func refresh_dirs():
	texture_dir = assets_dir + "assets/minecraft/textures/"
	sound_dir = assets_dir + "assets/minecraft/sounds/"
	music_dir = assets_dir + "assets/minecraft/sounds/music/"
	shader_dir = assets_dir + "assets/minecraft/shaders/"
	font_data_dir = assets_dir + "assets/minecraft/font/"
	font_texture_dir = assets_dir + "assets/minecraft/textures/font/"


func list_files_in_dir(path):
	var file_path = path + "/_list.json"
	if path.ends_with("/"):
		file_path = path + "_list.json"
	var file = File.new()
	if not file.file_exists(file_path):
		return list_files_in_dir_fallback(path)
	file.open(file_path, File.READ)
	var json = file.get_as_text()
	var dict : Dictionary = parse_json(json)
	var files : Array = dict["files"]
	return files


func list_files_in_dir_fallback(path):
	var directory = Directory.new()
	var files = []
	directory.open(path)
	directory.list_dir_begin()
	while true:
		var file = directory.get_next()
		if file == "":
			break
		elif not file.begins_with("."):
			files.append(file)
	return files


func get_cropped_texture(texture : Texture, region : Rect2) -> AtlasTexture:
	var atlas_texture := AtlasTexture.new()
	atlas_texture.set_atlas(texture)
	atlas_texture.set_region(region)
	return atlas_texture


func get_resized_texture(t, width: int, height: int):
	var image = t.get_data()
	if image == null:
		push_warning("Error resizing texture: Data is null")
		return t
	if width > 0 && height > 0:
		image.resize(width, height, 0)
	var itex = ImageTexture.new()
	itex.create_from_image(image, 1)
	return itex


func prepare_fonts():
	if settings["font_dir"] == null:
		settings["font_dir"] = "default"
		save_settings()
	if settings["font_dir"] == "default":
		var new_font = DynamicFont.new()
		new_font.font_data = DEFAULT_FONT
		font = new_font
	else:
		var file = File.new()
		if file.file_exists(settings["font_dir"]):
			var new_font_data = DynamicFontData.new()
			new_font_data.font_path = settings["font_dir"]
			var new_font = DynamicFont.new()
			new_font.font_data = new_font_data
			font = new_font
		else:
			push_warning("Could not load font at path %s" % settings["font_dir"])
			settings["font_dir"] = "default"
			save_settings()
			var new_font = DynamicFont.new()
			new_font.font_data = DEFAULT_FONT
			font = new_font


func save_settings():
	var file = File.new()
	file.open(SAVE_PATH_SETTINGS, File.WRITE)
	file.store_string(to_json(settings))
	file.close()


func reset_settings():
	var file = File.new()
	file.open(SAVE_PATH_SETTINGS, File.WRITE)
	file.store_string(to_json(DEFAULT_SETTINGS))
	file.close()


func load_settings():
	var file = File.new()
	if file.file_exists(SAVE_PATH_SETTINGS):
		file.open(SAVE_PATH_SETTINGS, File.READ)
		var raw_data = file.get_as_text()
		settings = parse_json(raw_data)
		if settings == null:
			settings = DEFAULT_SETTINGS
		if not compare_arrays(DEFAULT_SETTINGS.keys(), settings.keys()):
			print("Invalid config detected!")
			reset_settings()
	OS.window_fullscreen = settings["fullscreen"]


func compare_arrays(arr1: Array, arr2: Array) -> bool:
	for i in arr1:
		if not arr2.has(i):
			return false
	return true


func _input(_event):
	if Input.is_action_just_pressed("toggle_fullscreen"):
		settings["fullscreen"] = !OS.window_fullscreen
		OS.window_fullscreen = settings["fullscreen"]
		save_settings()


func missing_ressource():
	if not has_shown_broken_asset_pack_message:
		has_shown_broken_asset_pack_message = true
		OS.alert("Some ressources could not be found in the provided asset pack. They will be replaced with fallback ressources.", "Warning")


func not_yet_implemented():
	OS.alert("Sorry, but this feature has not yet been implemented.", "Warning")


func try(error: int):
	assert(error == OK)
