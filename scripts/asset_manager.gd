extends Control

const COPYING_MESSAGE = "Copying data..."
const CONNECTING_MESSAGE = "Connecting to server..."
const CONNECTING_FAILED_MESSAGE = "Could not connect to server."
const NOT_VALID_ASSET_PACK_MESSAGE = "Error: Not a valid asset pack."
const DOWNLOADING_MESSAGE = "Downloading assets..."
const EXTRACTING_MESSAGE = "Extracting..."
const FONT_IS_ZIP_WARNING = "The provided file looks like a zip. Please provide a font file instead."
const HTTP_ERROR = "Error requesting file: %s"
const LOADING_SPINNER_SPEED = 400

var copy_thread = Thread.new()
var target_files = []
var downloading := false
var downloading_font := false

onready var main_menu = $MainMenu
onready var main_menu_font = $MainMenuFont
onready var install_from_file = $InstallFromFile
onready var install_from_file_not_zip = $InstallFromFileNotZip
onready var install_from_file_font = $InstallFromFileFont
onready var download_menu = $DownloadMenu
onready var download_menu_font = $DownloadMenuFont
onready var download_url = $DownloadMenu/VBoxContainer/VBoxContainer/HBoxContainer/LineEdit
onready var download_url_font = $DownloadMenuFont/VBoxContainer/VBoxContainer/HBoxContainer/LineEdit
onready var valid_url_warning = $DownloadMenu/VBoxContainer/VBoxContainer/HBoxContainer/LineEdit/ValidUrlWarning
onready var valid_url_warning_font = $DownloadMenuFont/VBoxContainer/VBoxContainer/HBoxContainer/LineEdit/ValidUrlWarning
onready var loading_menu = $LoadingMenu
onready var loading_spinner = $LoadingMenu/HBoxContainer/LoadingAnchor/Spinner
onready var loading_message = $LoadingMenu/HBoxContainer/Title
onready var error_menu = $ErrorMenu
onready var error_message = $ErrorMenu/HBoxContainer/Title
onready var http = $HTTPRequest
onready var http2 = $HTTPRequest2
onready var error_timer = $ErrorTimer
onready var extract_zip = $ExtractZip


func _ready():
	Global.load_settings()
	# Check if assets have been set
	if Global.settings["assets_dir"] != null:
		Global.assets_dir = Global.settings["assets_dir"]
		Global.prepare_assets()
		Audio.autoplay_menu_music()
		Global.try(get_tree().change_scene("res://scenes/main_menu.tscn"))
	Global.try(get_tree().connect("files_dropped", self, "_on_files_dropped"))


func _process(delta):
	if loading_menu.visible:
		loading_spinner.rotation_degrees += delta * LOADING_SPINNER_SPEED
	if downloading:
		var status = http.get_http_client_status()
		if status == 6 or status == 3 or status == 1:
			loading_message.text = CONNECTING_MESSAGE
		elif status == 7:
			var downloaded_bytes = http.get_downloaded_bytes()
			var body_size = http.get_body_size()
			if body_size > 0:
				loading_message.text = DOWNLOADING_MESSAGE + " (" + str(round(100 * downloaded_bytes / body_size)) + "%)"
			else:
				loading_message.text = DOWNLOADING_MESSAGE + " (%s MB)" % round(downloaded_bytes / 1000000)
		elif status == 5:
			downloading = false
			loading_menu.hide()
			error_menu.show()
			error_message.text = CONNECTING_FAILED_MESSAGE
			error_timer.start()
	if downloading_font:
		var status = http2.get_http_client_status()
		if status == 6 or status == 3 or status == 1:
			loading_message.text = CONNECTING_MESSAGE
		elif status == 7:
			var downloaded_bytes = http2.get_downloaded_bytes()
			var body_size = http2.get_body_size()
			if body_size > 0:
				loading_message.text = DOWNLOADING_MESSAGE + " (" % str(round(100 * downloaded_bytes / body_size)) + "%)"
			else:
				loading_message.text = DOWNLOADING_MESSAGE + " (%s MB)" % round(downloaded_bytes / 1000000)
		elif status == 5:
			downloading_font = false
			loading_menu.hide()
			error_menu.show()
			error_message.text = CONNECTING_FAILED_MESSAGE
			error_timer.start()


func list_files_in_dir(dir):
	var directory = Directory.new()
	var files = []
	directory.open(dir)
	directory.list_dir_begin()
	while true:
		var file = directory.get_next()
		if file == "":
			break
		elif not file.begins_with("."):
			files.append(file)
	return files


func _on_files_dropped(files, _screen):
	if install_from_file.visible:
		print("Dropped files: %s" % files)
		if not are_files_zip(files):
			target_files = files
			install_from_file.hide()
			install_from_file_not_zip.show()
		else:
			install_from_file.hide()
			loading_menu.show()
			loading_message.text = COPYING_MESSAGE
			copy_files_async(files)
	elif install_from_file_font.visible:
		var path = copy_font(files)
		Global.settings["font_dir"] = path
		Global.save_settings()
		Global.prepare_assets()
		Audio.autoplay_menu_music()
		Global.try(get_tree().change_scene("res://scenes/main_menu.tscn"))


func copy_files_async(files):
	if copy_thread.is_active():
		copy_thread.wait_to_finish()
	copy_thread.start(self, "copy_files", files)


func copy_files(files):
	var d = Directory.new()
	var file_path
	for i in files:
		file_path = OS.get_user_data_dir() + "/assets.zip"
		if file_path != i:
			d.copy(i, file_path)
		else:
			push_warning("User attempted to copy existing file.")
	call_deferred("copy_done")
	return


func copy_font(files):
	var d = Directory.new()
	var file_path
	for i in files:
		file_path = "user://font"
		if file_path != i:
			d.copy(i, file_path)
		else:
			push_warning("User attempted to copy existing file.")
	return file_path


func copy_done():
	loading_message.text = EXTRACTING_MESSAGE
	extract_zip.ExtractAsync(OS.get_user_data_dir() + "/assets.zip", OS.get_user_data_dir() + "/" + get_next_assets_name())


func are_files_zip(files) -> bool:
	var val := true
	for i in files:
		if not i.ends_with("zip"):
			val = false
	return val


func _on_LoadFromFile_pressed():
	main_menu.hide()
	install_from_file.show()


func _on_DownloadFromUrl_pressed():
	main_menu.hide()
	download_menu.show()


func _on_InstallFromFile_Back_pressed():
	install_from_file.hide()
	main_menu.show()


func _on_DownloadMenu_Back_pressed():
	download_menu.hide()
	main_menu.show()


func _on_ChooseAnotherFile_pressed():
	install_from_file_not_zip.hide()
	install_from_file.show()


func _on_YesInstallFile_pressed():
	copy_files_async(target_files)
	install_from_file_not_zip.hide()
	loading_menu.show()
	loading_message.text = COPYING_MESSAGE


func _on_StartDownload_pressed():
	if download_url.text == "":
		valid_url_warning.show()
	else:
		download_menu.hide()
		loading_menu.show()
		downloading = true
		var error = http.request(download_url.text)
		if error != OK:
			loading_menu.hide()
			error_menu.show()
			error_message.text = HTTP_ERROR % error
			error_timer.start()


func _on_ErrorTimer_timeout():
	error_menu.hide()
	main_menu.show()


func _on_HTTPRequest_request_completed(_result, _response_code, _headers, _body):
	downloading = false
	loading_message.text = EXTRACTING_MESSAGE
	extract_zip.ExtractAsync(OS.get_user_data_dir() + "/assets.zip", OS.get_user_data_dir() + "/" + get_next_assets_name())


func get_next_assets_name() -> String:
	var directory = Directory.new()
	var files = []
	directory.open("user://")
	directory.list_dir_begin()
	while true:
		var file = directory.get_next()
		if file == "":
			break
		elif not file.begins_with("."):
			files.append(file)
	directory.list_dir_end()
	var i := 0
	while files.has(str(i)):
		i += 1
	return str(i)


func check_asset_path(dir):
	var files = list_files_in_dir(dir)
	if files.has("assets"):
		return dir
	else:
		# Some zip files create a directory with the assets
		for i in files:
			var new_path = dir + "/" + i
			var new_files = list_files_in_dir(new_path)
			if new_files.has("assets"):
				return new_path + "/"
	return null


func _on_ExtractZip_ExtractionFinished(file_path: String):
	print("Extraction finished!")
	file_path = check_asset_path(file_path)
	if file_path != null:
		Global.settings["assets_dir"] = file_path
		Global.save_settings()
		Global.assets_dir = file_path
		if Global.settings["font_dir"] != null:
			Global.prepare_assets()
			Audio.autoplay_menu_music()
			Global.try(get_tree().change_scene("res://scenes/main_menu.tscn"))
		else:
			loading_menu.hide()
			main_menu_font.show()
	else:
		loading_menu.hide()
		error_menu.show()
		error_message = NOT_VALID_ASSET_PACK_MESSAGE
		error_timer.start()
		


func _on_UseDefaultFont_pressed():
	Global.settings["font_dir"] = "default"
	Global.save_settings()
	Global.prepare_assets()
	Audio.autoplay_menu_music()
	Global.try(get_tree().change_scene("res://scenes/main_menu.tscn"))
	


func _on_Back_InstallFont_pressed():
	install_from_file_font.hide()
	main_menu_font.show()


func _on_FontLoadFromFile_pressed():
	main_menu_font.hide()
	install_from_file_font.show()


func _on_Back_DownloadFont_pressed():
	download_menu_font.hide()
	main_menu_font.show()


func _on_Back_StartDownload_pressed():
	# Start downloading font
	if download_url_font.text == "":
		valid_url_warning_font.show()
	elif download_url_font.text.ends_with(".zip"):
		valid_url_warning_font.show()
		valid_url_warning_font.text = FONT_IS_ZIP_WARNING
	else:
		download_menu_font.hide()
		loading_menu.show()
		downloading_font = true
		var error = http2.request(download_url_font.text)
		if error != OK:
			loading_menu.hide()
			error_menu.show()
			error_message.text = HTTP_ERROR % error
			error_timer.start()


func _on_HTTPRequest2_request_completed(_result, _response_code, _headers, _body):
	downloading_font = false
	Global.settings["font_dir"] = "user://font"
	Global.save_settings()
	Global.prepare_assets()
	Audio.autoplay_menu_music()
	Global.try(get_tree().change_scene("res://scenes/main_menu.tscn"))


func _on_DownloadFontFromUrl_pressed():
	main_menu_font.hide()
	download_menu_font.show()


func _exit_tree():
	if copy_thread.is_active():
		copy_thread.wait_to_finish()
