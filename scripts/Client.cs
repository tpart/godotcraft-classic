using Godot;
using classy.Network;
using classy.Packets;
using classy.Packets.Client;
using classy.Packets.Server;
using System.Net.Sockets;
using System;
using System.Threading;
using classy.Data;
using System.Collections.Generic;

public class Client : Node
{
    [Signal]
    delegate void ConnectionError(string Error);
    [Signal]
    delegate void ChunkDownloadingStart();
    [Signal]
    delegate void SetLoadingBar(int value);
    [Signal]
    delegate void GameStart();
    [Signal]
    delegate void TeleportPlayer(Godot.Vector3 Position);
    [Signal]
    delegate void SpawnPlayer(int PlayerId, Godot.Vector3 Position);
    [Signal]
    delegate void PlayerPositionUpdate(int PlayerId, Godot.Vector3 Position);
    [Signal]
    delegate void DespawnPlayer(int PlayerId);

    public MinecraftConnection Connection;
    public bool Connected = false;
    public bool GameStarted = false;
    public static Map Map = new Map();
    public TerrainGenerator TerrainGenerator;
    public KinematicBody Player;
    public float MaxTimeSinceLastPositionAndRotationUpdate = 0.1f;
    private float _timeSinceLastPositionAndRotationUpdate = 0f;
    private CancellationTokenSource _cancellationTokenSource;
    private CancellationToken _cancellationToken;
    private List<(short, short, short, Block)> _blockQueue = new List<(short, short, short, Block)>();

    public override void _Ready()
    {
        TerrainGenerator = GetParent().GetNode<TerrainGenerator>("TerrainGenerator");
        Player = GetParent().GetNode<KinematicBody>("Player");
    }

    public override void _PhysicsProcess(float delta)
    {
        if (GameStarted)
        {
            _timeSinceLastPositionAndRotationUpdate += delta;
            if (_timeSinceLastPositionAndRotationUpdate > MaxTimeSinceLastPositionAndRotationUpdate)
            {
                IPacket positionAndOrientationPacket = new classy.Packets.Server.PositionAndOrientationPacket
                {
                    PlayerId = 255,
                    X = (short)(Player.Translation.x * 32),
                    Y = (short)((Player.Translation.y + 0.65) * 32),
                    Z = (short)((Player.Translation.z + 1) * 32),
                    Yaw = (byte)(256 * -1 * (((Player.RotationDegrees.y + 180) > 360) ? Player.RotationDegrees.y + 180 - 360 : Player.RotationDegrees.y + 180) / 360),
                    Pitch = 0,
                };
                Connection.SendPacket(positionAndOrientationPacket);
            }
        }
    }

    public override void _Process(float delta)
    {
        if (_blockQueue.Count > 0)
        {
            for (int i = 0; i < _blockQueue.Count; i++)
            {
                (short, short, short, Block) blockInfo = _blockQueue[i];
                SetLocalBlock(blockInfo.Item1, blockInfo.Item2, blockInfo.Item3, blockInfo.Item4);
            }
            _blockQueue = new List<(short, short, short, Block)>();
        }
    }

    public void Connect(string ipAddress, ushort port, int protocolVersion, string playerName)
    {
        _cancellationTokenSource = new CancellationTokenSource();
        _cancellationToken = _cancellationTokenSource.Token;

        GD.Print("Starting Minecraft Client.");

        try
        {
            Connection = new MinecraftConnection(ipAddress, (int)port, _cancellationToken, ReceivedPacket, ReceivedMapPackets, UpdatePercentage);
        }
        catch (System.Net.Sockets.SocketException e)
        {
            EmitSignal("ConnectionError", e.ToString());
            return;
        }

        IPacket loginPacket = new PlayerIdentificationPacket
        {
            ProtocolVersion = 0x07,
            Username = "Steve",
            VerificationKey = "AXY8aCrqVjK43BfH",
        };

        Connection.SendPacket(loginPacket);
    }

    public void ReceivedPacket(IPacket packet)
    {
        if (packet is ServerIdentificationPacket serverIdentificationPacket)
        {
            GD.Print($"Connected to server: {serverIdentificationPacket.ServerName}");
        }
        else if (packet is LevelInitializePacket levelInitializePacket)
        {
            GD.Print("Starting map loading!");
            EmitSignal("ChunkDownloadingStart");
        }
        else if (packet is SpawnPlayerPacket spawnPlayerPacket)
        {
            if (spawnPlayerPacket.PlayerId < 0)
            {
                EmitSignal("TeleportPlayer", new Godot.Vector3(spawnPlayerPacket.X / 32, spawnPlayerPacket.Y / 32, spawnPlayerPacket.Z / 32));
            }
            else
            {
                EmitSignal("SpawnPlayer", spawnPlayerPacket.PlayerId, new Godot.Vector3(spawnPlayerPacket.X / 32, spawnPlayerPacket.Y / 32, spawnPlayerPacket.Z / 32));
            }
        }
        else if (packet is classy.Packets.Client.PositionAndOrientationPacket positionAndOrientationPacket)
        {
            if (positionAndOrientationPacket.PlayerId < 0)
            {
                EmitSignal("TeleportPlayer", new Godot.Vector3(positionAndOrientationPacket.X / 32, positionAndOrientationPacket.Y / 32, positionAndOrientationPacket.Z / 32));
            }
            else
            {
                EmitSignal("PlayerPositionUpdate", positionAndOrientationPacket.PlayerId, new Godot.Vector3((float)(positionAndOrientationPacket.X) / 32.0f, (float)(positionAndOrientationPacket.Y) / 32.0f - 0.5f, (float)(positionAndOrientationPacket.Z) / 32.0f - 1f));
            }
        }
        else if (packet is classy.Packets.Client.SetBlockPacket setBlockPacket)
        {
            Block oldBlock = (Block)Map.GetBlock(setBlockPacket.X, setBlockPacket.Y, setBlockPacket.Z);
            if (oldBlock != (Block)setBlockPacket.BlockType)
            {
                GD.Print($"Adding block {setBlockPacket.X}, {setBlockPacket.Y}, {setBlockPacket.Z} to queue");
                _blockQueue.Add((setBlockPacket.X, setBlockPacket.Y, setBlockPacket.Z, (Block)setBlockPacket.BlockType));
            }
        }
        else if (packet is DespawnPlayerPacket despawnPlayerPacket)
        {
            EmitSignal("DespawnPlayer", despawnPlayerPacket.PlayerId);
        }
        else if (packet is DisconnectPlayerPacket disconnectPlayerPacket)
        {
            EmitSignal("ConnectionError", disconnectPlayerPacket.DisconnectReason);
        }
        else
        {
            GD.Print($"Received unhandled packet ({packet.GetType()}");
        }
    }

    public void BreakBlock(Vector3 blockPos)
    {
        Map.SetBlock((short)blockPos.x, (short)blockPos.y, (short)blockPos.z, Block.Air);
        TerrainGenerator.UpdateBlock((short)blockPos.x, (short)blockPos.y, (short)blockPos.z, Map);

        IPacket setBlockPacket = new classy.Packets.Server.SetBlockPacket
        {
            X = (short)blockPos.x,
            Y = (short)blockPos.y,
            Z = (short)blockPos.z,
            Mode = 0x00,
            BlockType = (byte)Block.Air,
        };

        Connection.SendPacket(setBlockPacket);
    }

    public void PlaceBlock(Vector3 blockPos)
    {
        Map.SetBlock((short)blockPos.x, (short)blockPos.y, (short)blockPos.z, Block.Dirt);
        TerrainGenerator.UpdateBlock((short)blockPos.x, (short)blockPos.y, (short)blockPos.z, Map);

        IPacket setBlockPacket = new classy.Packets.Server.SetBlockPacket
        {
            X = (short)blockPos.x,
            Y = (short)blockPos.y,
            Z = (short)blockPos.z,
            Mode = 0x01,
            BlockType = (byte)Block.Dirt,
        };

        Connection.SendPacket(setBlockPacket);
    }

    public void SetLocalBlock(short blockX, short blockY, short blockZ, Block block)
    {
        Map.SetBlock(blockX, blockY, blockZ, block);
        TerrainGenerator.UpdateBlock(blockX, blockY, blockZ, Map);
    }

    public void ReceivedMapPackets(byte[] data, LevelFinalizePacket levelFinalizePacket)
    {
        Map.AddAllChunkData(data);
        Map.ProcessLevel(levelFinalizePacket.XSize, levelFinalizePacket.YSize, levelFinalizePacket.ZSize);
        GD.Print("Map loading finished.");
        TerrainGenerator.GenerateMap(Map);
        EmitSignal("GameStart");
        GameStarted = true;
    }

    public void UpdatePercentage(byte percentage)
    {
        EmitSignal("SetLoadingBar", percentage);
    }

    public void TerminateConnection()
    {
        GD.Print("Closing connection to server");
        _cancellationTokenSource.Cancel();
        Connection.Close();
    }
}