using classy.Packets;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using System;
using System.Collections.Generic;
using classy.Packets.Client;

namespace classy.Network
{
    public class MinecraftConnection
    {
        public MinecraftStream minecraftStream;
        private CancellationToken _cancellationToken { get; }
        private PacketRegistry packetRegistry = new PacketRegistry();
        public Thread NetworkThread { get; private set; }
        private ReceivedPacketCallBack _callBack;
        private ReceivedMapCallBack _mapCallBack;
        private UpdatePercentageCallBack _updatePercentageCallBack;
        private bool _readingChunks = false;
        private byte[] _mapDataPackets = new byte[0];

        public MinecraftConnection(string serverUrl, int port, CancellationToken cancellationToken, ReceivedPacketCallBack callBack, ReceivedMapCallBack mapCallBack, UpdatePercentageCallBack updatePercentageCallBack)
        {
            minecraftStream = new MinecraftStream(serverUrl, port);
            _cancellationToken = cancellationToken;
            _callBack = callBack;
            _mapCallBack = mapCallBack;
            _updatePercentageCallBack = updatePercentageCallBack;
            NetworkThread = new Thread(ProcessNetwork);
            NetworkThread.Name = "classy - Network Thread";
            NetworkThread.Start();
        }

        public delegate void ReceivedPacketCallBack(IPacket packet);
        public delegate void ReceivedMapCallBack(byte[] packets, LevelFinalizePacket levelFinalizePacket);
        public delegate void UpdatePercentageCallBack(byte percentage);
        private int _packetCount = 0;

#nullable enable
        public void ProcessNetwork()
        {
            try
            {
                SpinWait sw = new SpinWait();

                while (!_cancellationToken.IsCancellationRequested)
                {
                    if (_cancellationToken.IsCancellationRequested)
                        break;

                    if (minecraftStream.IsDataAvailable())
                    {
                        IPacket? packet = TryReadPacket(minecraftStream);
                        if (packet != null)
                        {
                            if (_readingChunks)
                            {
                                if (packet is LevelFinalizePacket levelFinalizePacket)
                                {
                                    _readingChunks = false;
                                    _mapCallBack(_mapDataPackets, levelFinalizePacket);
                                }
                                else if (packet is LevelDataChunkPacket levelDataChunkPacket)
                                {
                                    _mapDataPackets = Combine(_mapDataPackets, levelDataChunkPacket.ChunkData);
                                    _updatePercentageCallBack(levelDataChunkPacket.PercentComplete);
                                }
                                else
                                {
                                    Godot.GD.Print("Received non-chunk packet before map was loaded. This should not happen.");
                                    _callBack(packet);
                                }
                            }
                            else
                            {
                                if (packet is LevelInitializePacket levelInitializePacket)
                                {
                                    _readingChunks = true;
                                    _mapDataPackets = new byte[0];
                                }
                                _callBack(packet);
                            }
                        }
                    }
                    else
                    {
                        sw.SpinOnce();
                    }
                }
            }
            catch (Exception e)
            {
                Godot.GD.Print($"Error reading paket: {e.Message}");
            }
        }

        public static byte[] Combine(byte[] first, byte[] second)
        {
            byte[] ret = new byte[first.Length + second.Length];
            Buffer.BlockCopy(first, 0, ret, 0, first.Length);
            Buffer.BlockCopy(second, 0, ret, first.Length, second.Length);
            return ret;
        }

        public IPacket? TryReadPacket(MinecraftStream minecraftStream)
        {
            byte packetId = minecraftStream.ReadByte();
            IPacket? packet = packetRegistry.GetClientPacket(packetId);
            if (packet != null)
            {
                packet.Decode(minecraftStream);
                return packet;
            }
            else
            {
                Godot.GD.Print($"Received packet that has not yet been implemented. ID: {packetId}");
                return null;
            }
        }
#nullable disable

        public void SendPacket(IPacket packet)
        {
            byte? packetId = packetRegistry.GetServerId(packet);
            if (packetId != null)
            {
                minecraftStream.WriteByte((byte)packetId);
                packet.Encode(minecraftStream);
                minecraftStream.ClearBuffer();
            }
            else
            {
                Godot.GD.Print($"Could not send packet because it has not yet been implemented.");
            }
        }

        public void Close()
        {
            minecraftStream.Close();
        }
    }
}