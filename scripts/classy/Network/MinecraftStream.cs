﻿using System.Text;
using System.Net.Sockets;
using System;

namespace classy.Network
{
    public class MinecraftStream
    {
        public NetworkStream Stream;
        public TcpClient Client;
        private byte[] _buffer = new byte[0];

        public MinecraftStream(string serverUrl, int port)
        {
            Client = new TcpClient(serverUrl, port);
            Stream = Client.GetStream();
        }

        public byte ReadByte()
        {
            return (byte)Stream.ReadByte();
        }

        public sbyte ReadSByte()
        {
            return (sbyte)Stream.ReadByte();
        }

        public byte[] ReadByteArray()
        {
            return ReadBytes(1024);
        }

        public short ReadShort()
        {
            byte[] shortBytes = ReadBytes(2);
            Array.Reverse(shortBytes);
            return BitConverter.ToInt16(shortBytes, 0);
        }

        public int ReadInt()
        {
            byte[] intBytes = ReadBytes(4);
            Array.Reverse(intBytes);
            return BitConverter.ToInt32(intBytes, 0);
        }

        public string ReadString()
        {
            byte[] stringBytes = ReadBytes(64);
            return Encoding.ASCII.GetString(stringBytes).TrimEnd(' ');
        }

        public void WriteByte(byte data)
        {
            byte[] temp = new byte[_buffer.Length + 1];
            Buffer.BlockCopy(_buffer, 0, temp, 0, _buffer.Length);
            temp[_buffer.Length] = data;
            _buffer = temp;
        }

        public void WriteSByte(sbyte data)
        {
            byte[] temp = new byte[_buffer.Length + 1];
            Buffer.BlockCopy(_buffer, 0, temp, 0, _buffer.Length);
            temp[_buffer.Length] = (byte)data;
            _buffer = temp;
        }

        public void WriteByteArray(byte[] Send)
        {
            WriteBytes(Send);
        }

        public void WriteShort(short data)
        {
            byte[] shortBytes = BitConverter.GetBytes(data);
            Array.Reverse(shortBytes);
            WriteBytes(shortBytes);
        }

        public void WriteInt(int data)
        {
            byte[] intBytes = BitConverter.GetBytes(data);
            Array.Reverse(intBytes);
            WriteBytes(intBytes);
        }

        public void WriteString(string data)
        {
            data = data.PadRight(64, ' ');
            WriteBytes(Encoding.ASCII.GetBytes(data));
        }

        public bool IsDataAvailable()
        {
            return Stream.DataAvailable;
        }

        private byte[] ReadBytes(int size)
        {
            var data = new byte[size];
            int bytesRead = Stream.Read(data, 0, size);

            if (bytesRead == size)
            {
                return data;
            }

            while (true)
            {
                if (bytesRead != size)
                {
                    int newSize = size - bytesRead;

                    bytesRead = Stream.Read(data, bytesRead, newSize);

                    if (bytesRead != newSize)
                    {
                        size = newSize;
                    }
                    else
                    {
                        break;
                    }
                }
            }

            return data;
        }

        private void WriteBytes(byte[] bytes)
        {
            if (_buffer == null)
            {
                _buffer = bytes;
            }
            else
            {
                int tempLength = _buffer.Length + bytes.Length;
                byte[] temp = new byte[tempLength];

                Buffer.BlockCopy(_buffer, 0, temp, 0, _buffer.Length);
                Buffer.BlockCopy(bytes, 0, temp, _buffer.Length, bytes.Length);

                _buffer = temp;
            }
        }

        public void ClearBuffer()
        {
            try
            {
                Stream.Write(_buffer, 0, _buffer.Length);
                _buffer = new byte[0];
            }
            catch
            {
                Godot.GD.Print("Failed to send buffer!");
            }
        }

        public void Close()
        {
            Client.GetStream().Close();
            Client.Close();
        }
    }
}