using System.Collections.Generic;
using classy.Packets;
using classy.Packets.Client;
using classy.Packets.Server;


namespace classy.Packets
{
    class PacketRegistry
    {
        public Dictionary<byte, IPacket> packetRegistryClient = new Dictionary<byte, IPacket>();
        public Dictionary<byte, IPacket> packetRegistryServer = new Dictionary<byte, IPacket>();

        public PacketRegistry()
        {
            // Client packets (Server -> Client)
            packetRegistryClient.Add(0x00, new ServerIdentificationPacket());
            packetRegistryClient.Add(0x01, new PingPacket());
            packetRegistryClient.Add(0x02, new LevelInitializePacket());
            packetRegistryClient.Add(0x03, new LevelDataChunkPacket());
            packetRegistryClient.Add(0x04, new LevelFinalizePacket());
            packetRegistryClient.Add(0x06, new Client.SetBlockPacket());
            packetRegistryClient.Add(0x07, new SpawnPlayerPacket());
            packetRegistryClient.Add(0x08, new Client.PositionAndOrientationPacket());
            packetRegistryClient.Add(0x09, new PositionAndOrientationUpdatePacket());
            packetRegistryClient.Add(0x0a, new PositionUpdatePacket());
            packetRegistryClient.Add(0x0b, new OrientationUpdatePacket());
            packetRegistryClient.Add(0x0c, new DespawnPlayerPacket());
            packetRegistryClient.Add(0x0d, new Client.MessagePacket());
            packetRegistryClient.Add(0x0e, new DisconnectPlayerPacket());
            packetRegistryClient.Add(0x0f, new UpdateUserTypePacket());

            
            // Server packets (Client -> Server)
            packetRegistryServer.Add(0x00, new PlayerIdentificationPacket());
            packetRegistryServer.Add(0x05, new Server.SetBlockPacket());
            packetRegistryServer.Add(0x08, new Server.PositionAndOrientationPacket());
            packetRegistryServer.Add(0x0d, new Server.MessagePacket());
        }

#nullable enable
        public IPacket? GetClientPacket(byte packetId)
        {
            if (packetRegistryClient.ContainsKey(packetId))
                return packetRegistryClient[packetId];
            else
                return null;
        }

        public IPacket? GetServerPacket(byte packetId)
        {
            if (packetRegistryServer.ContainsKey(packetId))
                return packetRegistryServer[packetId];
            else
                return null;
        }
#nullable disable

        public byte? GetClientId(IPacket packet)
        {
            foreach (KeyValuePair<byte, IPacket> kvp in packetRegistryClient)
            {
                if (kvp.Value.GetType() == packet.GetType())
                {
                    return kvp.Key;
                }
            }
            return null;
        }

        public byte? GetServerId(IPacket packet)
        {
            foreach (KeyValuePair<byte, IPacket> kvp in packetRegistryServer)
            {
                if (kvp.Value.GetType() == packet.GetType())
                {
                    return kvp.Key;
                }
            }
            return null;
        }
    }
}