using classy.Network;


namespace classy.Packets.Client
{
    public class MessagePacket : IPacket
    {
        public sbyte PlayerId { get; set; }
        public string Message { get; set; } = "";
        

        public void Decode(MinecraftStream minecraftStream)
        {
            PlayerId = minecraftStream.ReadSByte();
            Message = minecraftStream.ReadString();
        }

        public void Encode(MinecraftStream minecraftStream)
        {
            minecraftStream.WriteSByte(PlayerId);
            minecraftStream.WriteString(Message);
        }
    }
}