using classy.Network;


namespace classy.Packets.Client
{
    public class PositionUpdatePacket : IPacket
    {
        public sbyte PlayerId { get; set; }
        public sbyte ChangeInX { get; set; }
        public sbyte ChangeInY { get; set; }
        public sbyte ChangeInZ { get; set; }

        public void Decode(MinecraftStream minecraftStream)
        {
            PlayerId = minecraftStream.ReadSByte();
            ChangeInX = minecraftStream.ReadSByte();
            ChangeInY = minecraftStream.ReadSByte();
            ChangeInZ = minecraftStream.ReadSByte();
        }

        public void Encode(MinecraftStream minecraftStream)
        {
            minecraftStream.WriteSByte(PlayerId);
            minecraftStream.WriteSByte(ChangeInX);
            minecraftStream.WriteSByte(ChangeInY);
            minecraftStream.WriteSByte(ChangeInZ);
        }
    }
}