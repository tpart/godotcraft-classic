extends Spatial

const OTHER_PLAYER_SCENE = preload("res://scenes/other_player.tscn")
const CLOUD_SPEED = 0.5

var other_players = {}

onready var player : KinematicBody = $Player
onready var in_game_ui = $Player/InGameUI
onready var client = $Client
onready var other_players_node = $OtherPlayers
onready var connecting_status = $Connecting/MarginContainer/VBoxContainer/Title
onready var loading_bar = $Connecting/MarginContainer/VBoxContainer/ProgressBar
onready var clouds = $Clouds
onready var connecting = $Connecting
onready var connecting_bg = $Connecting/Bg


func _ready():
	# Set up connecting screen
	var dirt = Global.load_image_texture(Global.texture_dir + "gui/options_background.png")
	connecting_bg.texture = Global.get_resized_texture(dirt, dirt.get_size().x * 4 * Global.settings["gui_scale"], dirt.get_size().y * 4 * Global.settings["gui_scale"])
	connecting.show()
	in_game_ui.hide()
	# Set up environment
	clouds.get_surface_material(0).albedo_texture = Global.load_image_texture(Global.texture_dir + "environment/clouds.png")
	# Connect signals
	client.connect("ConnectionError", self, "_on_connection_error")
	client.connect("GameStart", self, "_on_game_start")
	client.connect("ChunkDownloadingStart", self, "_on_chunk_downloading_start")
	client.connect("TeleportPlayer", self, "_on_telport_player")
	client.connect("SpawnPlayer", self, "_on_spawn_player")
	client.connect("PlayerPositionUpdate", self, "_on_player_position_update")
	client.connect("SetLoadingBar", self, "_on_set_loading_bar")
	client.connect("DespawnPlayer", self, "_on_despawn_player")
	# Connect to server
	client.Connect(Global.server_address, 25565, Global.PROTOCOL_VERSION, "testplayer")


func break_block(block_pos: Vector3):
	client.BreakBlock(block_pos)


func place_block(block_pos: Vector3):
	client.PlaceBlock(block_pos)


func _process(delta):
	clouds.translation.x += delta * CLOUD_SPEED


func _on_set_loading_bar(percentage: float):
	loading_bar.value = percentage


func _on_connection_error(error):
	Global.connection_error = error.split("\n")[0]
	Global.try(get_tree().change_scene("res://scenes/connection_status.tscn"))


func _on_game_start():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	connecting.hide()
	in_game_ui.show()


func _on_telport_player(pos):
	player.translation = pos


func _on_spawn_player(id, pos):
	print("creating player at %s" % pos)
	var other_player = OTHER_PLAYER_SCENE.instance()
	other_players_node.add_child(other_player)
	other_player.translation = pos
	other_players[id] = other_player


func _on_player_position_update(id, pos):
	if other_players.has(id):
		other_players[id].translation = pos


func _on_despawn_player(id):
	if other_players.has(id):
		other_players[id].queue_free()
		other_players.erase(id)
		print("Player %s despawned" % id)


func _on_chunk_downloading_start():
	connecting_status.text = "Loading chunks..."


func _on_TerrainGenerator_ChunksReady():
	player.start()


func terminate_connection():
	client.TerminateConnection()
