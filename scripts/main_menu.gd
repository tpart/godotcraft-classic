extends Control


var music_files = []
var current_music_index = 0

onready var l1 = $Title/Logo/L1
onready var l2 = $Title/Logo/L2
onready var splash = $Splash
onready var version = $Version
onready var version2 = $Version2
onready var skybox = $Skybox
onready var skybox_camera = $Skybox/Camera
onready var bg_blur = $BgBlur


func _ready():
	randomize()
	# Load logo
	l1.texture = Global.get_cropped_texture(Global.load_image_texture(Global.texture_dir + "gui/title/minecraft.png"), Rect2(Vector2(0, 0), Vector2(155, 44)))
	l2.texture = Global.get_cropped_texture(Global.load_image_texture(Global.texture_dir + "gui/title/minecraft.png"), Rect2(Vector2(0, 45), Vector2(119, 44)))
	l1.texture = Global.get_resized_texture(l1.texture, l1.texture.get_size().x * 2 * Global.settings["gui_scale"], l1.texture.get_size().y * 2 * Global.settings["gui_scale"])
	l2.texture = Global.get_resized_texture(l2.texture, l2.texture.get_size().x * 2 * Global.settings["gui_scale"], l2.texture.get_size().y * 2 * Global.settings["gui_scale"])
	# Set up font
	var splash_font = DynamicFont.new()
	splash_font.font_data = Global.font.font_data
	splash.add_font_override("font", splash_font)
	version.add_font_override("font", Global.font)
	version2.add_font_override("font", Global.font)
	# Load minecraft background skybox
	var files = Global.list_files_in_dir(Global.texture_dir + "gui/title/background/")
	var panorama_0
	var panorama_1
	var panorama_2
	var panorama_3
	var panorama_4
	var panorama_5
	for i in files:
		match i:
			"panorama_0.png":
				panorama_0 = Global.load_image_texture(Global.texture_dir + "gui/title/background/" + i)
			"panorama_1.png":
				panorama_1 = Global.load_image_texture(Global.texture_dir + "gui/title/background/" + i)
			"panorama_2.png":
				panorama_2 = Global.load_image_texture(Global.texture_dir + "gui/title/background/" + i)
			"panorama_3.png":
				panorama_3 = Global.load_image_texture(Global.texture_dir + "gui/title/background/" + i)
			"panorama_4.png":
				panorama_4 = Global.load_image_texture(Global.texture_dir + "gui/title/background/" + i)
			"panorama_5.png":
				panorama_5 = Global.load_image_texture(Global.texture_dir + "gui/title/background/" + i)
	skybox.set_up_textures(panorama_0, panorama_1, panorama_2, panorama_3, panorama_4, panorama_5)
	skybox_camera.rotation_degrees.y = [0, 90, 180, 270][randi() % 4]
	# Set up blur
	var blur_json = Global.load_json(Global.shader_dir + "post/blur.json")
	if not blur_json.empty():
		bg_blur.material.set_shader_param("blurSize", blur_json["passes"][0]["uniforms"][1]["values"][0])
	# Load splash
	var splash_file = File.new()
	var splash_file_path = Global.assets_dir + "assets/minecraft/texts/splashes.txt"
	if splash_file.file_exists(splash_file_path):
		splash_file.open(Global.assets_dir + "assets/minecraft/texts/splashes.txt", File.READ)
		var line_count = 0
		while not splash_file.eof_reached():
			line_count += 1
			splash_file.get_line()
		splash_file.seek(0)
		var splash_line = randi() % line_count
		var splash_text = ""
		for _i in range(0, splash_line):
			splash_text = splash_file.get_line()
		splash_file.close()
		splash.text = splash_text
		yield(get_tree(), "idle_frame")
		splash.rect_pivot_offset = splash.rect_size * 0.5
		splash_font.size = int(28 - splash_text.length() * 0.25)
	# Version text
	version.text = "Godotcraft Classic " + str(Global.VERSION)
	version2.text = "Compatible with Minecraft %s" % str(Global.MC_VERSION)


var t = 0
func _physics_process(delta):
	# Camera rotation
	t += delta
	skybox_camera.rotation_degrees.y -= 0.03
	skybox_camera.rotation.x = cos(t * 0.1) * -0.2
	# Splash text animation
	splash.rect_scale = Vector2(1, 1) + Vector2(sin(t * 12), sin(t * 12)) * 0.025


func _on_multiplayer_pressed():
	Audio.play_sound(Global.load_sound(Global.sound_dir + "random/click_stereo.ogg"))
	Global.try(get_tree().change_scene("res://scenes/multiplayer_menu.tscn"))


func _on_quit_game_pressed():
	get_tree().quit()


func _on_Singleplayer_pressed():
	Global.not_yet_implemented()


func _on_Options_pressed():
	Global.not_yet_implemented()
